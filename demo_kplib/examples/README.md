Examples 
---

This folder contains examples of using the executable `demo_kplib` to generate VASP format KPOINTS file.

The command is (take 1118_triclinic as example):

    $ cd 1118_triclinic
    $ ../../build/demo_kplib ./POSCAR ./PRECALC > KPOINTS

The results from `demo_kplib` is `KPOINTS_kplib`. The reference results `KPOINTS_ref` are from our stand-alone Java application ***K*-point Grid Generator** (http://muellergroup.jhu.edu/K-Points.html).
