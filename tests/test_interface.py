import os
import pytest
import numpy as np
from kpLib.interface import get_kpoints
from pymatgen.io.vasp.inputs import Poscar, Kpoints

TEST_DIR = os.path.join(os.path.dirname(__file__))


test_systems = ["cubic", "triclinic", "ortho", "ortho_shifted", "hex"]


@pytest.mark.parametrize("system", test_systems)
def test_kpLib(system):
    poscar = Poscar.from_file(os.path.join(TEST_DIR, f"POSCAR_{system}"))
    kpoints = Kpoints.from_file(os.path.join(TEST_DIR, f"KPOINTS_{system}"))
    ref_kpts_and_weights = np.concatenate(
        [np.array(kpoints.kpts), np.array([kpoints.kpts_weights]).T], axis=1
    )

    struc = poscar.structure
    include_gamma = True if "shifted" not in system else False

    kpts = get_kpoints(struc, minDistance=24.9, include_gamma=include_gamma)

    # Get distinct points and weights for KPOINTS file
    distinct_coords, distinct_weights = zip(
        *[
            (coord, weight)
            for (coord, weight) in zip(kpts["coords"], kpts["weights"])
            if weight > 0
        ]
    )

    gen_kpts_and_weights = np.concatenate(
        [np.array(distinct_coords), np.array([distinct_weights]).T], axis=1
    )

    np.array_equal(ref_kpts_and_weights, gen_kpts_and_weights)
